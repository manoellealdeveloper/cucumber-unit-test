package br.com.cucumber.steps;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import org.junit.Assert;

import br.com.cucumber.entity.Filme;
import br.com.cucumber.entity.NotaAluguel;
import br.com.cucumber.services.AluguelService;
import cucumber.api.DataTable;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;

public class LocadoraSteps {
	
	private Filme filme;
	private AluguelService aluguelService = new AluguelService();
	private NotaAluguel nota;
	private String error;
	private String tipoAluguel;
	
	@Dado("^um filme com estoque de (\\d+) unidades$")
	public void umFilmeComEstoqueDeUnidades(int unidade) {
		filme = new Filme();
		filme.setEstoque(unidade);
	}
	
	@Dado("^um filme$")
	public void umFilme(DataTable table) {
	    Map<String, String> map = table.asMap(String.class, String.class);
	    filme = new Filme();
	    filme.setEstoque(Integer.parseInt(map.get("estoque")));
	    filme.setAluguel(Integer.parseInt(map.get("preco")));
	}

	@Dado("^que o preço de aluguel seja R\\$ (\\d+)$")
	public void queOPreçoDeAluguelSejaR$(int aluguel){
		filme.setAluguel(aluguel);	
	}

	@Quando("^alugar$")
	public void alugar() {
		try {
			 nota = aluguelService.alugar(filme, tipoAluguel);
		} catch (RuntimeException e) {
			error = e.getMessage();
		}

	}

	@Entao("^o preço do aluguel será R\\$ (\\d+)$")
	public void oPreçoDoAluguelSeráR$(int preco) {
		Assert.assertEquals(preco, nota.getPreco());
	}

	@Entao("^a data de entrega será no dia seguinte$")
	public void aDataDeEntregaSeráNoDiaSeguinte(){
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, 1);
		
		Date dataRetorno = nota.getDataEntrega();
		Calendar calRetorno = Calendar.getInstance();
		calRetorno.setTime(dataRetorno);
		
		Assert.assertEquals(cal.get(Calendar.DAY_OF_MONTH), calRetorno.get(Calendar.DAY_OF_MONTH));
		Assert.assertEquals(cal.get(Calendar.MONTH), calRetorno.get(Calendar.MONTH));
		Assert.assertEquals(cal.get(Calendar.YEAR), calRetorno.get(Calendar.YEAR));
	}

	@Entao("^o estoque do filme será (\\d+) unidade$")
	public void oEstoqueDoFilmeSeráUnidade(int arg1) {
		Assert.assertEquals(arg1, filme.getEstoque());
	}

	@Entao("^não será possível por falta de estoque$")
	public void nãoSeráPossívelPorFaltaDeEstoque() {
	    Assert.assertEquals("Filme sem estoque", error);
	}

	@Dado("^que o tipo do aluguel seja (.*)$")
	public void queOTipoDoAluguelSejaExtendido(String tipo) throws Throwable {
	    tipoAluguel = tipo;
	}

	@Entao("^a data de entrega será em (\\d+) dias$")
	public void aDataDeEntregaSeráEmDias(int arg1) throws Throwable {
			
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DAY_OF_MONTH, arg1);
			Date dataEsperada = cal.getTime();
			Date dataReal = nota.getDataEntrega();
			
			DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
			
			Assert.assertEquals(format.format(dataEsperada), format.format(dataReal));
		
	}

	@Entao("^a pontuação recebida será de (\\d+) pontos$")
	public void aPontuaçãoRecebidaSeráDePontos(int arg1) throws Throwable {
	   Assert.assertEquals(nota.getPontuacao(), arg1);
	}
	


}
