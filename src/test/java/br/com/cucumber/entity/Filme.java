package br.com.cucumber.entity;

public class Filme {
	
	private int estoque;
	private int aluguel;

	public int getAluguel() {
		return aluguel;
	}

	public void setAluguel(int aluguel) {
		this.aluguel = aluguel;
	}

	public void setEstoque(int estoque) {
		this.estoque = estoque;
	}

	public int getEstoque() {
		return estoque;
	}
	

}
